require 'lib/myexam.rb'

describe Myexam do

       before :each do
	@p1 = Myexam::Myexam.new("Cual de estas carreras se da en la etsii?", {a: "informatica", b: "magisterio", c: "civil", d: "todas las anteriores"})
end
		
	describe "# Almacenamiento de las preguntas y respuestas" do
		
	it " hay una pregunta" do
		@p1.pregunta.should eq("Cual de estas carreras se da en la etsii?")

       	end

	it "hay varias respuestas" do 
		@p1.respuestas.should eq({a:"informatica", b:"magisterio", c:"civil", d:"todas las anteriores"})

	end
	end
	
	describe "mostrar por pantalla la pregunta y sus respuestas" do
	it "mostrar por pantalla" do
	@p1.to_s.should eq("Cual de estas carreras se da en la etsii?\na) informatica\nb) magisterio\nc) civil\nd) todas las anteriores\n")
end
end

end
